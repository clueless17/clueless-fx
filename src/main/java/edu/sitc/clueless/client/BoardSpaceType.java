package edu.sitc.clueless.client;

/**
 * A simple Enumeration representing whether a <code>BoardSpace</code> object is a Room or a Hallway.
 * @author Robert
 */
public enum BoardSpaceType {
	HALLWAY,
	ROOM,
	EMPTY
}
