package edu.sitc.clueless.client;

/**
 * A class that stores the map of the board.
 * @author Robert
 */
public class BoardMap {
	private final MapSkin skin;
	private final Tile[][] tiles = new Tile[5][5];
	
	public BoardMap() {
		int blanks = 1;
		
		for (int x = 0; x < tiles.length; x++) {
			for (int y = 0; y < tiles[x].length; y++) {
				if (x % 2 == 0 || y % 2 == 0) {
					
				} else {
					tiles[x][y] = new Tile("blank_" + blanks++);
				}
			}
		}
		
		this.skin = new MapSkin(this);
	}
	
	public int getWidth() {
		return this.tiles.length;
	}
	
	public int getHeight() {
		return this.tiles[0].length;
	}
}
