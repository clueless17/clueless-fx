package edu.sitc.clueless.client;

import javafx.scene.layout.GridPane;

/**
 * A visual skin for a <code>BoardMap</code>. Extends
 * <code>javafx.scene.layout.GridPane</code>, which uses CSS for styling.
 * @author Robert
 */
public class MapSkin extends GridPane {
	/**
	 * Simple constructor.
	 * @param map <code>BoardMap</code> for which to generate the skin.
	 */
	public MapSkin(BoardMap map) {
		// Add the 'game-board' class.
		getStyleClass().add("game-board");
		
		for (int x = 0; x < map.getWidth(); x++) {
			for (int y = 0; y < map.getHeight(); y++) {
//				add(map.getTile(x, y).getSkin(), x, y);
			}
		}
	}
}
