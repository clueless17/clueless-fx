package edu.sitc.clueless.client;

/**
 * Represents a single tile on the Clue-Less board.
 * @author Robert
 */
public class Tile {
	private final String id;
	private final BoardSpaceType type;
	
	/**
	 * Create a <code>Tile</code> with the given ID and default type
	 * (<code>EMPTY</code>).
	 * @param id The ID <code>String</code> for this <code>Tile</code>.
	 */
	public Tile(String id) {
		this(id, BoardSpaceType.EMPTY);
	}
	
	/**
	 * Create a <code>Tile</code> with the given ID and type.
	 * @param id The ID <code>String</code> for this <code>Tile</code>.
	 * @param tileType The <code>BoardSpaceType</code> to use for this
	 * <code>Tile</code>.
	 */
	public Tile(String id, BoardSpaceType tileType) {
		this.id = id;
		this.type = tileType;
	}
	
	/**
	 * Get the ID of this <code>Tile</code>.
	 * @return ID <code>String</code>.
	 */
	public String getId() {
		return this.id;
	}
	
	/**
	 * Get the type of this <code>Tile</code>.
	 * @return <code>BoardSpaceType</code> representing the type of
	 * <code>Tile</code> this is.
	 */
	public BoardSpaceType getType() {
		return this.type;
	}
}
